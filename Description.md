Alias Analysis Proof of Concept
===============================
 
This repository contains a proof of concept implementation of the alias anaysis algorithm that I worked with for my master's thesis.
 
It is written in Haskell.
 
The algorithm is an implementation of the may-point-to algorithm presented by John Whaley and Martin Rinard in their paper *Compositional Pointer and Escape Analysis for Java Programs*.

As part of my thesis work I complemented their algorithm with a must-point to analysis. See the thesis for more details. It is availible in the download section of this project page.
