{-# LANGUAGE OverloadedStrings #-}

module PointsTo.Test (
    module PointsTo.Alg,
    module PointsTo.Test,
  ) where

import PointsTo.Alg
import PointsTo.Util
import PointsTo.Data


maxIter :: Int
maxIter = 10


runTestCase :: TestCase -> IO ()
runTestCase tCase = do
  _ <- runTestCaseGraph tCase
  return ()

runTestCaseGraph :: TestCase -> IO PointsToGraph
runTestCaseGraph tCase = do
  if tcDisable tCase then do
    putStrLn ("Test case disabled: " ++ tcName tCase)
    return emptyGraph
  else do
    printStr "-----------------------------------------------------------------"
    printStr ("Test case name: " ++ tcName tCase)
    printVal "classes" (tcClasses tCase) ; printVal "finals" (tcFinals tCase) ; printNl
    
    graph  <- analyse (tcProg tCase) (tcClasses tCase) (tcFinals tCase) maxIter tCase
    
    printStr "--------- Result ----------"
    printVal "graph" graph
    printNl
    return graph
  

