{-# LANGUAGE OverloadedStrings #-}
{-# OPTIONS_GHC -fno-warn-missing-signatures #-}

module PointsTo.TestCases (
    module PointsTo.Test,
    module PointsTo.TestCases,
  ) where

import PointsTo.Alg
import PointsTo.Data
import PointsTo.Operations

import PointsTo.Test


import qualified Data.Set as S

allTests = [
    aliasCondTest, 
    aliasTest,
    loopManyTest,
    loopNestedTest,
    aliasDiffTest,
    aliasDiffNeg1Test,
    aliasDiffNeg2Test,
    aliasCondNestedTest,
    storeTest,
    strongTest,
    breakTest,
    continueTest,
    loadTest1,
    loadTest2,
    invTest,
    condVarNegTest,
    loadFinalAliasTest,
    loopLoadTest,
    loopSwapTest,
    loopNestedCondSwapTest,
    ifSwapTest,
    ifTest,
    propTest,
    copyTest,
    returnTest,
    noAliasTest,
    ifLoopTest,
    ifNotEqTest,
    ifNotEqNullTest,
    ifCondCopyTest,
    loopNodesEqTest,
    aliasMergeNestedTest,
    modLoadTest,
    strongLoopTest,
    loopStoreTest,
    loopIfTest,
    loopSwap3Test,
    aliasAssignIfTest,
    mergeRefLoopTest,
    loopSmallNestedCondSwapTest
  ]

testAll = do
  mapM_ runTestCase allTests
  putStrLn "All tests completed without errors"
  


aliasCondTest = mkTest{
  tcName = "Cond alias",
  tcProg = Method ["p1", "p2"] [
        Create "l1",
        Create "l2",
        Create "l5",
        Copy "l4" "l2",
        
        If [ Copy "l1" "l2" ] [], -- l1 point to either n1 or n2
        
        If [ Copy "l2" "l5" ] [], -- l2 may alias l5 afterwards
        
        Copy "l3" "l1",

        If [
          EqConstr "l1" "l2",
          AssertMust   "l1" "l2", -- Alias between cond vars
          AssertMust   "l1" "l4", -- Must alias something eq to l2
          AssertNotMay "l2" "l5" -- May not alias l5 in here when l2 == l1   
        ] [
          Copy "l1" "l2" -- Make sure l1 == l2 afterwards
        ],
        
        AssertMust "l1" "l2",
        AssertMay "l1" "l3" -- Not must, l1 might eq n1 and be assigned n2 later.
      ]
    }

aliasAssignIfTest = mkTest{
    tcDisable = True,
    tcName = "Alias assign if",
    tcProg = Method ["p1", "p2"] [
        Copy "l1" "p1",
        
        If [
          Copy "l2" "p1"
        ] [
          Copy "l2" "p2"
        ],
        
        If [
          EqConstr "l1" "l2",
          AssertMust "l2" "p1",
            
          If [
            EqConstr "p1" "p2",
            -- This test does not work. I think it's to tricky to handle.
            AssertMust "l2" "p1"
          ] []
        ] []
      ]
  }

aliasTest = mkTest{
    tcName = "Alias",
    tcProg = Method ["p1", "p2"] [
        Copy "l1" "p1",
        Copy "l6" "p1",

        If [
          Copy "l2" "p1"
        ] [
          Copy "l2" "p2",
          Copy "l6" "p1"
        ],
        
        Copy "l3" "l2",
        Copy "l4" "l3",
        
        Create "l5",
        
        AssertMust   "l2" "l4", -- Trans alias with mult nodes
        AssertMay    "p1" "l4", -- Not must alias with one
        AssertNotMay "l5" "l4", -- No aliases with fresh obj
        AssertMust   "l6" "p1"  -- Still aliases after join
      ]
  }

noAliasTest = mkTest{
    tcName = "No alias",
    tcProg = Method [] [
        Create "l3",
        Create "l4",

        If [
          Copy "l1" "l3",
          Copy "l2" "l4"
        ] [
          Copy "l1" "l4",
          Copy "l2" "l3"
        ],
        
        AssertMay "l1" "l2" -- They never actually alias here but algo can see that 
      ]
  }

ifLoopTest = mkTest{
    tcName = "If loop",
    tcProg = Method [] [
        Create "l1",
        Create "l2",
        Create "l3",
        Create "l4",
        
        If [ 
          Copy "l1" "l3"
        ] [
          Copy "l2" "l3"
        ],
        
        Loop [
        
          Copy "l3" "l1",
          Copy "l1" "l2",
          Copy "l2" "l3",
        
          If [
            EqConstr "l1" "l2",
            Copy "l3" "l1",
            Copy "l1" "l2",
            Copy "l2" "l3",
            
            Loop [
              Copy "l3" "l1",
              Copy "l1" "l2",
              Copy "l2" "l3"
            ],
            Copy "l3" "l1",
            Copy "l1" "l2",
            Copy "l2" "l3"
            
          ] [],
          
          Copy "l3" "l1",
          Copy "l1" "l2",
          Copy "l2" "l3"
        ],
        
        AssertMay "l1" "l2"
      ]
  }


appendListTest = mkTest{
    tcName = "Append list",
    tcClasses = [],
    
    tcProg = Method ["list", "e"] [
        Create "n",
        Store "n" "elem" "e",
        
        If [
          Return "n"
        ] [],
        
        Copy "p" "list",
        
        Loop [
          Load "p" "p" "next"
        ],
        
        Load "t1" "list" "next",
        AssertNotMay "t1" "n",
        
        Store "p" "next" "n",

        AssertMay "t1" "n",
        
        Return "list"
      ]
  }


aliasDiffTest = mkTest{
    tcName = "Alias diff branch",
    tcProg = Method ["p1", "p2"] [
        Create "l1",
        Create "l2",
        
        If [
          Copy "l1" "p1",
          Copy "l2" "p1"
        ] [
          Copy "l1" "p2",
          Copy "l2" "p2"
        ],
        
        AssertMust "l1" "l2" -- Equal MergeRefValue:s created for both l1 and l2 
      ]
  }

aliasDiffNeg1Test = mkTest{
    tcName = "Alias neg 1 diff branch",
    tcProg = Method ["p1", "p2"] [
        
        If [
          Copy "l1" "p1"
        ] [
          Copy "l1" "p2"
        ],
        
        If [
          Copy "l2" "p1"
        ] [
          Copy "l2" "p2"
        ],
        -- Both points to similar MergeRefValue:s, but from different statements
        AssertMay "l1" "l2" 
      ]
  }

aliasDiffNeg2Test = mkTest{
    tcName = "Alias neg 2 diff branch",
    tcProg = Method ["p1", "p2"] [
        
        If [
          Copy "l1" "p1",
          Copy "l2" "p2"
        ] [
          Copy "l1" "p2",
          Copy "l2" "p1"
        ],
        
        AssertMay "l1" "l2" -- Similar UnionRefValue:s, but ref vals in different order 
      ]
  }

ifNotEqTest = mkTest{
    tcName = "If not eq",
    tcProg = Method ["p1", "p2"] [
      If [
        EqConstr "p1" "p2",
        AssertMust "p1" "p2"
      ] [
        NotEqConstr "p1" "p2",
        AssertNotMay "p1" "p2"
      ],
      
      AssertMay "p1" "p2"
    ]
  }


ifNotEqNullTest = mkTest{
    tcName = "If not eq null test",
    tcProg = Method ["p1", "p2"] [
      -- Pretend p2 is null. This is a common idiom for null checks that the algo handles.
      If [
        EqConstr "p1" "p2",
        AssertMust "p1" "p2",
        Return "p1"
      ] [
        NotEqConstr "p1" "p2",
        AssertNotMay "p1" "p2"
      ],
      
      -- p1 is known to be non null.
      AssertNotMay "p1" "p2"
    ]
  }


ifCondCopyTest = mkTest{
  tcName = "If cond copy",
  tcProg = Method ["p1", "p2"] [
        
        If [
          EqConstr "p1" "p2"
        ] [
          Copy "p1" "p2"
        ],
        
        AssertMust "p1" "p2"
      ]
    }

ifCondCopyThirdTest = mkTest{
  tcName = "If cond copy third",
  tcProg = Method ["p1", "p2", "p3"] [
        
        If [
          EqConstr "p1" "p2"
        ] [
          Copy "p1" "p3"
        ],
        
        AssertMust "p1" "p2"
      ]
    }


loopNodesEqTest = mkTest{
  tcName = "Loop nodes equal",
  tcProg = Method [] [
        Create "l1",
        Create "l2",
        Create "l3",
        Create "l4",
        
        If [Copy "l1" "l4"] [],
        
        Copy "l2" "l1",
        Copy "l3" "l1",
        
        If [Copy "l4" "l1"] [],
        
        -- Here all vars point to same two nodes, and all but l4 have l1's ref val.
        -- Loop would terminate immediately if fixedpoint of nodes was used, leaving
        -- MustAlias(l1, l2) even if it should be removed. 
        Loop [
          Copy "l1" "l2",
          Copy "l2" "l3",
          Copy "l3" "l4"
        ],
        
        AssertMay "l1" "l2",
        AssertMay "l1" "l3",
        AssertMay "l1" "l4"
      ]
    }

aliasCondNestedTest = mkTest{
  tcName = "Cond nested alias",
  tcProg = Method ["p1", "p2"] [
        Create "l1",
        Create "l2",
        Create "l3",
        Create "l4",

        If [Copy "l3" "l4"] [], -- l3 may point to         l3, l4
        If [Copy "l2" "l3"] [], -- l2 may point to     l2, l3, l4
        If [Copy "l1" "l2"] [], -- l1 may point to l1, l2, l3, l4

        If [
          EqConstr "l1" "l2",
          If [
            EqConstr "l2" "l3",
            PrintGraph,
            AssertMust "l1" "l2",
            AssertMust "l2" "l3",
            AssertMust "l3" "l1", -- The tricky point, recursive intersection computation
            AssertMay  "l1" "l4"
          ] [],
          AssertMust "l1" "l2",
          AssertMay  "l2" "l3",
          AssertMay  "l3" "l1"
        ] [],
        
        AssertMay "l1" "l2"
      ]
    }


condVarNegTest = mkTest{
  tcName = "Cond variables negative test",
  tcProg = Method ["p1", "p2"] [
        Copy "l1" "p1",
        Copy "l2" "p2",

        If [
          EqConstr "p1" "p2",
          AssertMust "p1" "p2",
          AssertMay "l1" "l2" -- This is a clear alias but the algorithm can't see it
        ] []
      ]
    }


-- I don't come up with a counter example here...
aliasMergeNestedTest = mkTest{
  tcName = "Alias merge nested",
  tcProg = Method ["p1", "p2"] [
        Create "l1",
        Create "l2",
        Create "l3",
        Create "l4",

        If [Copy "l3" "l4"] [], -- l3 may point to         l3, l4
        If [Copy "l2" "l3"] [], -- l2 may point to     l2, l3, l4
        If [Copy "l1" "l2"] [], -- l1 may point to l1, l2, l3, l4

        If [
          EqConstr "l1" "l2",
          If [
            EqConstr "l2" "l3",
            AssertMust "l1" "l2",
            AssertMust "l2" "l3",
            AssertMust "l3" "l1",
            AssertMay "l1" "l4"
          ] [],
          AssertMay "l3" "l1",
          AssertMust "l1" "l2",
          AssertMay "l2" "l3"
        ] []
        
      ]
    }

loadFinalAliasTest = mkTest{
    tcName   = "Load final alias",
    tcFinals = ["f1"],
    tcProg = Method ["p1", "p2"] [
      Copy "l1" "p1",
      
      Load "l2" "p1" "f1",
      Load "l3" "l1" "f1",
      AssertMust "l2" "l3", -- Finals from same obj
      
      Load "l4" "p1" "f2",
      Load "l5" "l1" "f2",
      AssertMay "l4" "l5", -- Non final from same obj

      Load "l6" "p2" "f1",
      Load "l7" "l1" "f1",
      AssertMay "l6" "l7" -- Finals from different objs could be same
    ]
  }


modLoadTest = mkTest{
    tcName = "Mod load",
    tcProg = Method ["p1", "p2"] [
      Copy "l1" "p1",
      If [] [Copy "l1" "p2"],
      Load "l2" "l1" "f"
    ]
  }

loopLoadTest = mkTest{
    tcName = "Loop load",
    tcProg = Method ["p1", "p2"] [
      Copy "l2" "p2",
      Loop [
        Load "l1" "p1" "f",
        Copy "l2" "l1"
      ],
      AssertMay "l1" "l2"
    ]
  }

strongLoopTest = mkTest{
    tcName = "Strong loop, singularity",
    tcProg = Method [] [
      Create "l2",
      Create "l3",
  
      Loop [
        Create "l1",
        
        Store "l1" "f" "l2",
  
        -- Here we know that all objs repr by l1 node will point to l2.
        -- This could be a strong update by an extesion? Only INSIDE the loop.
        -- We know nothing outside, loop may not be executed.
        Store "l1" "f" "l3",
  
        -- One of the objects of l1 will be updated, not the others.
        -- Therefor can not remove any edges.
        If [] [ Store "l1" "f" "l3" ]
      ]
    ]
  }

strongTest = mkTest{
    tcName      = "Strong",
    tcProg = Method ["p1"] [
      Create "l1",
      Create "l2",
  
      Store "l1" "f1" "p1",
      Store "l1" "f1" "l2", -- Strong update, p1 will disappear
      
      PrintGraph,
      
      Assert 1 $ \g -> 
        let [n] = S.toList (pointedToVar (inEdges g) "l1")
        in S.size (pointedToField (inEdges g) n "f1"),
  
      Store "p1" "f2" "l1",
      Store "p1" "f2" "l2" -- Not strong update, p1 is outside node
    ]
  }

continueTest = mkTest{
    tcName = "Continue",
    tcProg = Method ["p"] [
      Create "l1",
      Create "l2",

      Loop [
        If [
          Copy "l3" "l1"
        ] [
          Copy "l3" "l2",
          Continue -- Continue ensures than node l2 does not escape. 
        ],
        Store "p" "f" "l3" -- l3 will never point to node l2 here.
      ],

      Assert False (\g -> isEscaped g (S.findMax (pointedToVar (inEdges g) "l2"))),
      Assert True (\g -> isEscaped g (S.findMax (pointedToVar (inEdges g) "l1")))
    ]
  }

breakTest = mkTest{
      tcName = "Break",
      tcProg = Method ["p1"] [
      Create "l1",
      Create "l2",
      Loop [
        Store "p1" "f" "l1",
        
        If [] [
          Copy "l1" "l2", -- l2 would escape if not for the break. 
          Break
        ]
      ],
      
      Assert False (\g -> isEscaped g (S.findMax (pointedToVar (inEdges g) "l2")))
    ]
  }

loopStoreTest = mkTest{
    tcName = "Loop store",
    tcProg = Method ["p1"] [
      Create "l1",
      Create "l2",
      Loop [
        Store "p1" "f" "l1",
        Copy "l1" "l2" -- l2 escapes the second iteration
      ]
      -- l1 still has an edge to its inside node
    ]
  }

loopManyTest = mkTest{
    tcName = "Loop many vars",
    tcProg = Method [] [
      Create "l1",
      Create "l2",
      Create "l3",
      Create "l4",
      Create "l5",
      
      Loop [
        Copy "l1" "l2",
        Copy "l2" "l3", 
        Copy "l3" "l4", 
        Copy "l4" "l5",
        AssertMay "l1" "l5" -- Test that tests use final result.
      ],
      -- All should be aliases
      AssertMay "l2" "l3",
      AssertMay "l1" "l2",
      AssertMay "l4" "l1",
      AssertMay "l1" "l5"
      -- l1 still has an edge to l1 node
    ]
  }

ifSwapTest = mkTest{
    tcName = "If swap",
    tcProg = Method [] [
      Create "l1",
      Create "l2",
      Create "l3",
      
      If [
        Copy "l3" "l1",
        Copy "l1" "l2",
        Copy "l2" "l3"
      ] [],
      
      -- l1 must aliases normally (mashes gone!)  
      Copy "l4" "l1",
      AssertMust "l1" "l4",

      -- All should be may-aliases
      AssertMay "l2" "l3",
      AssertMay "l1" "l2",
      AssertMay "l3" "l2"
    ]
  }
  
loopSwapTest = mkTest{
    tcName = "Loop swap",
    tcProg = Method [] [
      Create "l1",
      Create "l2",
      Create "temp",
      
      Loop [
        Copy "temp" "l1",
        Copy "l1" "l2",
        Copy "l2" "temp"
      ],
      
      Copy "l3" "l1",
      AssertMust "l1" "l3",

      -- All should be aliases
      AssertMay "l2" "temp",
      AssertMay "l1" "l2",
      AssertMay "temp" "l2"
      -- l1 still has an edge to l1 node
    ]
  }

loopSwap3Test = mkTest{
    tcName = "Loop swap 3",
    tcProg = Method [] [
      Create "l1",
      Create "l2",
      Create "l3",
      Create "l4",
      Create "temp",
      
      Loop [
--        PrintGraph,
        Copy "temp" "l1",
        Copy "l1" "l2",
        Copy "l2" "l3",
        Copy "l3" "l4",
        Copy "l4" "temp"
--        PrintGraph
      ],
      
      Copy "l5" "l1",
      AssertMust "l1" "l5",

      -- All should be aliases
      AssertMay "l2" "l3",
      AssertMay "l1" "l2",
      AssertMay "l3" "l2"
      -- l1 still has an edge to l1 node
    ]
  }    

loopSmallNestedCondSwapTest = mkTest{
    tcName = "Loop small nested cond swap",
    tcProg = Method [] [
      Create "l1",
      Create "l2",
      Create "l3",
      
      Loop [
        Loop [
          Copy "l3" "l1",
          Copy "l1" "l2",
          Copy "l2" "l3"
        ]
      ],

      AssertMay "l2" "l3",
      AssertMay "l1" "l2"
    ]
  }

loopNestedCondSwapTest = mkTest{
    tcName = "Loop nested cond swap",
    
    tcProg = Method [] [
      Create "l1",
      Create "l2",
      Create "l3",
      Create "l4",
      Create "l5",
      
      Loop [
        Copy "l1" "l2",
        Copy "l2" "l3", -- l1 will point to l3 the second iteration
        Loop [
          Copy "l3" "l4", -- l1 will point to l3 the second iteration
          Copy "l4" "l5", -- l1 will point to l3 the second iteration
          Copy "l5" "l1"
        ]
      ],
      -- All should be aliases
      AssertMay "l2" "l3",
      AssertMay "l1" "l2",
      AssertMay "l4" "l1",
      AssertMay "l1" "l5"
      -- l1 still has an edge to l1 node
    ]
  }

loopNestedTest = mkTest{
    tcName = "Loop nested",
    tcProg = Method [] [
      Create "l1",
      Create "l2",
      Create "l3",
      Create "l4",
      Create "l5",
      
      Loop [
        Copy "l1" "l2",
        Copy "l2" "l3", -- l1 will point to l3 the second iteration
        Loop [
          Copy "l3" "l4", -- l1 will point to l3 the second iteration
          Copy "l4" "l5",  -- l1 will point to l3 the second iteration
          AssertMay "l1" "l5" -- Test that tests use final result.
        ]
      ],
      -- All should be aliases
      AssertMay "l2" "l3",
      AssertMay "l1" "l2",
      AssertMay "l4" "l1",
      AssertMay "l1" "l5"
      -- l1 still has an edge to l1 node
    ]
  }

loopIfTest = mkTest{
    tcName = "Loop if",
    tcProg = Method ["p1"] [
      Create "l1",
      Create "l2",
      
      Loop [
        If [
          EqConstr "l1" "p1" 
        ] [
          Copy "l1" "l2"
        ]
      ]
      
      -- The real test is that it terminates      
    ]
  }

storeTest = mkTest{
      tcName = "Store simple",
      tcProg = Method ["p1"] [
        Create "l1",
        Copy "l2" "l1",
        Store "p1" "f" "l2",
        
        Assert True (\g -> isEscaped g (S.findMax (pointedToVar (inEdges g) "l2")))
      ]
  }

loadTest1 = mkTest{
    tcName = "Load simple 1",
    tcProg = Method ["p1"] [
--      Load "l1" "p1" "f",  -- p1 is escaped => Load node generated
      Create "l1",
      Create "l2",
      Store "l2" "f" "l1",
      Load "l3" "l2" "f", -- No load node generated
      
      Assert 1 $ \g -> 
        let n = S.findMax (pointedToVar (inEdges g) "l2")
        in S.size (pointedToField (inEdges g) n "f"), 
      AssertMust "l1" "l3"
    ]
  }

loadTest2 = mkTest{
    tcName = "Load simple 2",
    tcProg = Method ["p1", "p2"] [
      Copy "l1" "p1",
      
      If [] [ Copy "l1" "p2" ],
      Load "l2" "l1" "f"
--      Load "l2" "l1" "f"
--      Create "l3",
--      Store "l2" "f" "l3"
    ]
  }

invTest = mkTest{
    tcName = "Invokation",
    tcProg = Method [] [
      Create "l1",
      Create "l2",
      Create "l3",
      Create "l4", -- Will not escape
      Inv "l5" "op" ["l1", "l2"], -- Params escapes
      Store "l5" "f" "l3", -- l3 escaped
      
      Assert True (\g -> isEscaped g (S.findMax (pointedToVar (inEdges g) "l1"))),
      Assert True (\g -> isEscaped g (S.findMax (pointedToVar (inEdges g) "l3")))
    ]
  }


propTest = mkTest{
    tcName = "Propagate",
    tcProg = Method [] [
      Create "l1",
      Create "l2",
      Create "l3",
      Create "l4",
      
      Store "l2" "f" "l3",
      Store "l1" "f" "l2",
      
      Store "l4" "f" "l2", -- Should not escape
      
      Store "c1" "f" "l1"  -- One node escapes, escapedness propagates to all the others
    ]
  }

returnTest = mkTest{
    tcName = "Return",
    tcProg = Method ["p1", "p2"] [
      If [
          Copy "l1" "p1",
          Return "l1"  -- The graph from this branch is discarded
      ] [
          Copy "l1" "p2"
      ]
    ]
  }

ifTest = mkTest{
    tcName = "If",
    tcProg = Method ["p1", "p2"] [
      If [
        Copy "l1" "p1"
      ] [
        Copy "l1" "p2"
      ]
    ]
  }

copyTest = mkTest{
    tcName    = "Copy",
    tcClasses = ["c1"],
    
    tcProg =  Method ["p1"] [
      Copy "l1" "p1",
      Copy "l2" "l1",
      Copy "l1" "c1"
    ]
  }


mergeRefLoopTest = mkTest{
    tcName    = "Merge refs two tracks in loop",
    tcProg =  Method ["p1", "p2", "p3", "p4", "p5", "p6", "p7", "p8"] [
      If [
        Copy "p1" "p4"
      ] [
        Copy "p1" "p8"      
      ],
      
      Copy "p2" "p1",
      Copy "p3" "p1",
      
      If [] [
        Copy "p4" "p1",
        Copy "p8" "p1"
      ],
      
      Copy "p5" "p1",
      Copy "p6" "p1",
      Copy "p7" "p1",
      
      Loop [
        Copy "p1" "p2",
        Copy "p2" "p3",
        Copy "p3" "p4",
        
        Copy "p5" "p6",
        Copy "p6" "p7",
        Copy "p7" "p8"
      ],
      
      AssertMay "p1" "p5"
    ]
  }

{- 

Explanation of "Merge refs two tracks in loop"-test:

Tries to trick termination checker by a loop that could terminate before all must-aliases have been removed. Uses two "tracks" of variables.

loop:
  l1 = l2
  l2 = l3
  l3 = l4

  l5 = l6
  l6 = l7
  l7 = l8

MustAlias(l1, l5)?
  

-- Initial:
l1 -> 1
l2 -> 1
l3 -> 1
l4 -> 2

l5 -> 1
l6 -> 1
l7 -> 1
l8 -> 3


-- After body iteration 1:
l1 -> 1
l2 -> 1
l3 -> 2

l5 -> 1
l6 -> 1
l7 -> 3

-- After join iteration 1:
l1 -> 1
l2 -> 1
l3 -> M(1 2)

l5 -> 1
l6 -> 1
l7 -> M(1 3)


-- After body iteration 2:
l1 -> 1
l2 -> M(1 2)
l3 -> 2

l5 -> 1
l6 -> M(1 3)
l7 -> 3


-- After join iteration 2:
l1 -> 1
l2 -> M(1 M(1 2))
l3 -> M(1 2)

l5 -> 1
l6 -> M(1 M(1 3))
l7 -> M(1 3)

-}



--  void ex(Obj p) {
--    Obj l1 = new Obj();
--    Obj l2 = new Obj();
--    
--    l1.f = l2;
--    l1.f = p;
--    
--    if (...) l2 = p;
--    
--    l2.f = l1
--    Obj l3 = l2.f
--  }
  
  
loadStoreEx = mkTest{
    tcName = "Loop a few vars",
    tcProg = Method ["p"] [
      Create "l1",
      Create "l2",
      
      Store "l1" "f" "l2",
      Store "l1" "f" "p",
      
      If [
        Copy "l2" "p"
      ] [],
      
      Store "l2" "f" "l1",
      Load "l3" "l2" "f"
    ]
  }
  
loopAfewEx = mkTest{
    tcName = "Loop a few vars",
    tcProg = Method [] [
      Create "l1",
      Create "l2",
      Create "l3",
      
      Loop [
        Copy "l1" "l2",
        Copy "l2" "l3"
      ],
      -- All should be may-aliases
      AssertMay "l2" "l3",
      AssertMay "l1" "l2",
      AssertMay "l1" "l3"
    ]
  }
  