{-# LANGUAGE OverloadedStrings, ExistentialQuantification, StandaloneDeriving #-}
{-# LANGUAGE DeriveDataTypeable #-}

{-# OPTIONS_GHC -fno-warn-orphans #-}

module PointsTo.Data where

import GHC.Exts( IsString(..) )

import qualified Data.Map as M
import qualified Data.Set as S
import Data.Data
import qualified Data.ByteString.Char8 as B


import Prelude hiding (any, elem)

import Data.List (intercalate)
import Data.Foldable

-----------------------------------------

instance Show (a -> b) where
  show _f = "\\x.t"

-- The toy language

data Method =
  Method [Var] [Term]
  deriving (Show)

data MethodBlock = MethodBlock [Var] Term
  deriving Show

data Term =
  Load Var Var Field |
  Store Var Field Var |
  Copy Var Var |
  Create Var |
  Return Var |
  Inv Var Op [Var] |
  
  If [Term] [Term] |
  EqConstr Var Var |
  NotEqConstr Var Var |
  Loop [Term] |
  
  Continue |
  Break |
  Empty |
  
  PrintAlias Var Var |
  PrintGraph | 

  AssertMust Var Var |
  AssertMay Var Var |
  AssertNotMay Var Var |
  
  forall a. (Show a, Eq a) => Assert a (PointsToGraph -> a) |

  -- These are only used to write examples for thesis, 
  -- where block is not includes in If, Loop etc.
  Block [Term] |
  IfBlock Term Term |
  WhileBlock Term
  

deriving instance Show Term


-------------------------------------
-- Var

data Id =
  Id B.ByteString
  deriving (Eq, Ord, Data, Typeable)

instance IsString Id where
  fromString = Id . B.pack

instance Show Id where
  show (Id n) = show n

data Var =
  Var Id
  deriving (Eq, Ord, Data, Typeable)

instance Show Var where
  show (Var i) = show i

instance IsString Var where
  fromString = Var . fromString

data Field =
  Field Id
  deriving (Eq, Ord, Data, Typeable)

instance Show Field where
  show (Field i) = show i

instance IsString Field where
  fromString = Field . fromString

data Op =
  Op Id
  deriving (Eq, Ord, Data, Typeable)

instance Show Op where
  show (Op i) = show i

instance IsString Op where
  fromString = Op . fromString

type SiteIx = String

-------------------------------------------------------------------------------

isSingular :: Node -> Bool
isSingular (ParNode _)         = True 
isSingular (ClsNode _)         = True
isSingular (InNode   _ sing _) = sing
isSingular (LoadNode _ sing _) = sing 
isSingular (RetNode  _ sing _) = sing 

data Node =
  ParNode Var |
  ClsNode Var |
  InNode   SiteIx Bool String |
  LoadNode SiteIx Bool String |
  RetNode  SiteIx Bool String
  deriving (Eq, Ord, Show, Data, Typeable)


isInNode :: Node -> Bool
isInNode (InNode _ _ _) = True
isInNode _              = False

isOutNode :: Node -> Bool
isOutNode = not . isInNode

 -- Hack to get rid of nested "s. Don't use them in identifiers.
filterQuote :: String -> String
filterQuote =
  filter (/= '"')

mkInDesc :: Var -> String
mkInDesc var =
  filterQuote (show var)

mkLoadDesc :: Var -> Var -> Field -> String
mkLoadDesc v1 v2 f =
  filterQuote (show v1 ++ "=" ++ show v2 ++ "." ++ show f)

mkRetDesc :: Var -> Op -> [Var] -> String
mkRetDesc v op args = 
  filterQuote (show v ++ "=" ++ show op ++ "(" ++ intercalate "," (map show args) ++ ")")

---------------

data EscapeSite =
  VarSite Var |
  LoadSite SiteIx |
  InvSite SiteIx String
  deriving (Eq, Ord, Show, Data, Typeable)

data AlgContext = 
  AlgContext{
      acSiteIx       :: SiteIx,
      acExecOnce     :: Bool,
      acFinalFields  :: S.Set Field,
      acDoAsserts    :: Bool,
      acIterDone     :: Bool,
      acMaxLoopIter  :: Int,
      acTestCaseName :: String
    }
  deriving (Show, Data, Typeable)


initContext :: AlgContext
initContext = 
  AlgContext{ 
      acSiteIx       = "",
      acIterDone     = True,
      acMaxLoopIter  = 10,
      acExecOnce     = True,
      acFinalFields  = S.empty,
      acDoAsserts    = True,
      acTestCaseName = "Empty test case name"
    }

addSiteIx :: AlgContext -> SiteIx -> AlgContext
addSiteIx cont i = cont{ acSiteIx = acSiteIx cont ++ i }

isFinalField :: AlgContext -> Field -> Bool
isFinalField cont field = elem field (acFinalFields cont)

-- Things for site index as UID.
-- type Uid = String

-- unsafeUidRef :: IORef Int
-- unsafeUidRef = unsafePerformIO $ newIORef 1
-- 
-- genUid :: AlgContext -> IO String
-- genUid _cont = do
--   u <- readIORef unsafeUidRef
--   writeIORef unsafeUidRef (u+1)
-- --  u <- readIORef (uidRef cont)
-- --  writeIORef (uidRef cont) (u+1)
--   return ("u" ++ show u) 
-- 
-- genUids :: AlgContext -> Int -> IO [String]
-- genUids cont n = sequence (replicate n (genUid cont)) 


data RefValue =
  -- These contain info only to make them unique:
  RefValue SiteIx |          -- Plain old ref value
  NodeRefValue Node SiteIx | -- The most unique value possible
  FieldRefValue Node Field | -- Used for final fields of outside nodes
  
  -- These contain some info that is used in the analysis:
  -- r = ExclRefValue is r1 r2 => r created in place of r1 and excludes r2 
  ExclRefValue SiteIx RefValue RefValue |
  ConstrRefValue RefValue RefValue | -- Fields are always ordered: small-large
  MergeRefValue  SiteIx RefValue RefValue -- This must be a list because of ordering
  deriving (Eq, Ord, Data, Typeable)

-- deriving instance Show RefValue
instance Show RefValue where
  show = refToStr

refToStr :: RefValue -> String
refToStr ref = "\"" ++ go ref ++ "\""
  where
    go r = case r of
      RefValue sIx               -> "R" ++ sIx
      MergeRefValue _s  m1 m2 -> "M(" ++ go m1 ++ " " ++ go m2 ++ ")"
      ConstrRefValue c1 c2       -> "C(" ++ go c1 ++ " " ++ go c2 ++ ")"
      ExclRefValue _s e1 e2      -> "E(" ++ go e1 ++ " " ++ go e2 ++ ")"
      NodeRefValue n si          -> filterQuote $ "N(" ++ si ++ " " ++ show n ++ ")"
      FieldRefValue  n f         -> filterQuote $ "F(" ++ show n ++ " " ++ show f ++ ")"
      -- _ -> error "Showing unexpexted ref val"


mkConstrRefValue :: RefValue -> RefValue -> RefValue
mkConstrRefValue r1 r2 = 
  if r1 <= r2 then ConstrRefValue r1 r2
  else ConstrRefValue r2 r1

excludingRefVal :: RefValue -> RefValue
excludingRefVal (ExclRefValue _ er1 _er2) = er1
excludingRefVal r = error ("Unexpected ref val in exclRefVal: " ++ show r)

-- Union between var node-fields. Used in edge sets.
data Ref =
  FieldRef Node Field |
  VarRef Var
  deriving (Eq, Ord, Show, Data, Typeable)


-------------------------------------------------------------------------------

-- Graph

data PointsToGraph =
  PointsToGraph{
      outEdges  :: M.Map Ref (RefValue, S.Set Node),
      inEdges   :: M.Map Ref (RefValue, S.Set Node),
      escapeMap :: M.Map Node [EscapeSite],
      returned  :: S.Set Node
    }
  deriving (Eq, Show, Data, Typeable)


emptyGraph :: PointsToGraph
emptyGraph =
  PointsToGraph{
      outEdges = M.empty,
      inEdges = M.empty,
      
      escapeMap  = M.empty,
      returned   = S.empty
    }


-------------------------------------------------------------------------------

data AlgResult =
  AlgResult{
      arBreakGraphs    :: [PointsToGraph],
      arContinueGraphs :: [PointsToGraph],
      arReturnGraphs   :: [PointsToGraph]
    }
  deriving (Show, Data, Typeable)



emptyResult :: AlgResult
emptyResult =
  AlgResult{
      arBreakGraphs    = [],
      arContinueGraphs = [],
      arReturnGraphs   = []
    }


-------------------------------------------------------------------------------

-- The following is for testing 

data TestCase = 
  TestCase{
      tcDisable   :: Bool,
      tcName      :: String,
      tcProg      :: Method,
      tcClasses   :: [Var],
      tcFinals    :: [Field],
      tcDoAsserts :: Bool
      -- Removed because cannot have func in when useing Typeable
--      testFuncs      :: [PointsToGraph -> IO (Maybe String)]
    }
  deriving (Show)


mkTest :: TestCase
mkTest = TestCase{
    tcName        = error "No test name provided!", 
    tcFinals      = [],
    tcClasses     = [],
    tcDisable     = False,
    tcDoAsserts   = True,
    tcProg        = error "No test program provided!"
  }

