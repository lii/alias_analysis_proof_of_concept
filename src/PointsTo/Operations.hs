module PointsTo.Operations where

import Data.List hiding (any, foldl, concat, elem, concatMap, union, and, foldl')
import Prelude hiding (any, foldl, concat, elem, concatMap, exp, and)

import qualified Data.Map as M
import qualified Data.Set as S
import Data.Foldable

import PointsTo.Data
import PointsTo.Util

-- Graph equivalence relation. Graphs are equivalent if everyting is equal except merged
-- reference values, which must be created at the same statement. Used in loop analysis.
graphsEquiv :: PointsToGraph -> PointsToGraph -> SiteIx -> Bool
graphsEquiv graph1 graph2 siteIx =
     edgesEquiv (outEdges graph1) (outEdges graph2) siteIx
  && edgesEquiv (inEdges  graph1) (inEdges  graph2) siteIx
  && escapeMap graph1 == escapeMap graph2
  && returned  graph1 == returned  graph2

edgesEquiv :: M.Map Ref (RefValue, S.Set Node) -> M.Map Ref(RefValue, S.Set Node) -> SiteIx -> Bool
edgesEquiv es1 es2 siteIx = 
  (and 
    (M.mergeWithKey
      (\_var (ref1, nodes1) (ref2, nodes2) ->
        Just (refsEquiv ref1 ref2 siteIx && nodes1 == nodes2))
  --          (\m -> error ("edgesEquiv: Var in only right branch, should not happen: \n" ++ groom m))
  --          (\m -> error ("edgesEquiv: Var in only left branch, should not happen: \n" ++ groom m))
      (M.map (const True))
      (M.map (const True))
      -- (error "edgesEquiv: Var in only right branch, should not happen")
      es1
      es2))


refsEquiv :: RefValue -> RefValue -> SiteIx -> Bool
refsEquiv (MergeRefValue siteIx1 _ _) (MergeRefValue siteIx2 _ _) sIx 
  | siteIx1 == sIx && siteIx2 == sIx = True
refsEquiv r1 r2 _sIx = r1 == r2


-------------------------------------------------------------------------------


-- edgesFrom(I, n)
edgesFromNode :: [(Ref, Node)] -> Node -> [(Ref, Node)]
edgesFromNode edges node =
  [e | e@(FieldRef n _f, _n) <- edges, n == node]


-- I(v)
pointedToVar :: M.Map Ref (RefValue, S.Set Node) -> Var -> S.Set Node
pointedToVar edges var =
  lookupNodes (VarRef var) edges


-- I(n, f)
pointedToField :: M.Map Ref (RefValue, S.Set Node) -> Node -> Field -> S.Set Node
pointedToField edges node field =
  lookupNodes (FieldRef node field) edges


lookupNodes :: Ref -> M.Map Ref (RefValue, S.Set Node) -> S.Set Node
lookupNodes r m = 
  case M.lookup r m of
    Just (_refv, nodes) -> nodes
    Nothing -> S.empty

-------------------------------------------------------------------------------


-- Might the nodes correspond to the same object? Symmetric, reflexive.
maybeSameObj :: PointsToGraph -> Node -> Node -> Bool
maybeSameObj graph n1 n2 =
  -- Any two outside nodes might be the same. An escaped inside node might be same as
  -- any outside node. But two escaped inside nodes can not be the same as each other.
  n1 == n2
  || (isEscaped graph n1 && isOutNode n2)
  || (isEscaped graph n2 && isOutNode n1)


-- The set of nodes from any of the argument sets which correspond to an object which also
-- corresponds to a node in the other argument set. That is:
-- for all n1 in s1: elem n1 (interNodes s1 s2) iff exists n2 so that (maybeSameObj n1 n2) 
interNodes :: PointsToGraph -> S.Set Node -> S.Set Node -> S.Set Node
interNodes graph nodes1 nodes2 =
  S.filter (\n -> any (maybeSameObj graph n) nodes2) nodes1


-------------------------------------------------------------------------------

isConstr :: RefValue -> RefValue -> Bool
isConstr simpRef (ConstrRefValue r1 r2) =
  simpRef == r1 || simpRef == r2 || isConstr simpRef r1 || isConstr simpRef r2

isConstr _ _ = False


-- Tests if refLeft excludes refRight from possible may-aliasing.
isAliasExcluded :: RefValue -> RefValue -> Bool
isAliasExcluded _refLeft@(ExclRefValue _ r11 r12) refRight@(ExclRefValue _ r21 _r22) = 
     r12 == refRight
  || r12 == r21
  -- Not needed?
--  || isAliasExcluded refLeft r21
  -- Needed?
  || isAliasExcluded r11 refRight
  || isAliasExcluded r11 r21 
     
isAliasExcluded simpRef (ExclRefValue _ r1 r2) =
  simpRef == r1 || simpRef == r2

isAliasExcluded _r1 _r2 = False


-- Tests if refs are a mutual Constr <-> Excl pair, so they can be removed at join.
isMutualExclusion :: RefValue -> RefValue -> Bool
isMutualExclusion r1 r2 =
  case (r1, r2) of
    (ExclRefValue _ er1 er2, ConstrRefValue cr1 cr2) -> 
      er1 == cr1 || er1 == cr2 && er2 == cr1 || er2 == cr2
    _ -> False


-------------------------------------------------------------------------------

-- Since before excluded ref
-- mayAlias :: PointsToGraph -> Var -> Var -> Bool
-- mayAlias graph var1 var2 = let
--     pSet1 = pointedToVar (inEdges graph) var1
--     pSet2 = pointedToVar (inEdges graph) var2
--   in
--     not (S.null (interNodes graph pSet1 pSet2))

mayAlias :: PointsToGraph -> Var -> Var -> Bool
mayAlias graph var1 var2 = let
    (refVal1, pSet1) = lookupBold (VarRef var1) (inEdges graph)
    (refVal2, pSet2) = lookupBold (VarRef var2) (inEdges graph)
  in
    not (
      S.null (interNodes graph pSet1 pSet2)
        || isAliasExcluded refVal1 refVal2
        || isAliasExcluded refVal2 refVal1) 

mustAlias :: PointsToGraph -> Var -> Var -> Bool
mustAlias graph var1 var2 = let
    (r1, pSet1) = lookupBold (VarRef var1) (inEdges graph)
    (r2, pSet2) = lookupBold (VarRef var2) (inEdges graph)
  in
    r1 == r2 || isConstr r1 r2 || isConstr r2 r1
    -- Or same singleton set of singular node
    || (pSet1 == pSet2
      && S.size pSet1 == 1
      && isSingular (S.findMax pSet1))

-------------------------------------------------------------------------------

isEscaped :: PointsToGraph -> Node -> Bool
isEscaped graph node = not (null (lookupList node (escapeMap graph)))

isCaptured :: PointsToGraph -> Node -> Bool
isCaptured g n = not (isEscaped g n)

-------------------------------------------------------------------------------

joinEdges :: M.Map Ref (RefValue, S.Set Node) -> M.Map Ref (RefValue, S.Set Node) -> SiteIx
  -> M.Map Ref (RefValue, S.Set Node)
joinEdges edges1 edges2 siteIx =
  -- TODO: Could remove vars/fields that are in only one branch?  
  M.unionWith (joinEntry siteIx) edges1 edges2

joinEntry :: SiteIx -> (RefValue, S.Set Node) -> (RefValue, S.Set Node) -> (RefValue, S.Set Node)    
joinEntry siteIx ent1@(refVal1, nodes1) ent2@(refVal2, nodes2) =
  if refVal1 == refVal2 then ent1
  else if isConstr          refVal1 refVal2 then ent1
  else if isConstr          refVal2 refVal1 then ent2
  else if isMutualExclusion refVal1 refVal2 then (excludingRefVal refVal1, nodes1)
  else if isMutualExclusion refVal2 refVal1 then (excludingRefVal refVal2, nodes2)
  else (MergeRefValue siteIx refVal1 refVal2, S.union nodes1 nodes2)

joinResults :: AlgResult -> AlgResult -> AlgResult
joinResults res1 res2 = AlgResult{
    arReturnGraphs   = arReturnGraphs   res1 ++ arReturnGraphs   res2,
    arBreakGraphs    = arBreakGraphs    res1 ++ arBreakGraphs    res2,
    arContinueGraphs = arContinueGraphs res1 ++ arContinueGraphs res2 
  }

joinGraph :: SiteIx -> PointsToGraph -> PointsToGraph -> PointsToGraph
joinGraph siteIx graph1 graph2 = PointsToGraph{
    outEdges  = joinEdges (outEdges graph1) (outEdges graph2) siteIx,
    inEdges   = joinEdges (inEdges  graph1) (inEdges  graph2) siteIx,
    escapeMap = M.unionWith union (escapeMap graph1) (escapeMap graph2),
    returned  = S.union (returned graph1) (returned graph2)
  }


joinGraphs :: [PointsToGraph] -> SiteIx -> PointsToGraph
joinGraphs graphs siteIx =
  foldl' (joinGraph siteIx) emptyGraph graphs

-------------------------------------------------------------------------------
