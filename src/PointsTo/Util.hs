{-# LANGUAGE OverloadedStrings, 
  MultiParamTypeClasses, FlexibleInstances, FunctionalDependencies #-}

module PointsTo.Util where

import qualified Data.Map as M
import qualified Data.Set as S
import Data.Foldable
import qualified Data.List as L
import Prelude hiding (any, foldl, concat, elem, exp, all)
import Data.Char hiding (intToDigit)
import Language.Haskell.Exts.Parser
import Language.Haskell.Exts.Pretty


import PointsTo.Data

-- Enable or disable printouts that are done by debug functions
doPrintouts :: Bool
doPrintouts = False


-- Used to be able to treat Maps and Sets in a uniform way. Usefull when one wants to read
-- maps and sets that have been written with derived Show.
class Collection c a | c -> a where
  insert    :: a -> c -> c
  empty     :: c
  union     :: Eq a => c -> c -> c
  toList    :: c -> [a]
  fromList  :: [a] -> c  

instance Ord k => Collection (M.Map k v) (k, v) where
  insert (k, v) m = M.insert k v m 
  empty           = M.empty
  union           = M.union
  toList          = M.toList
  fromList        = M.fromList

instance Ord a => Collection (S.Set a) a where
  insert   = S.insert 
  empty    = S.empty
  union    = S.union
  toList   = S.toList
  fromList = S.fromList

instance Collection [a] a where
  insert   = (:)
  empty    = []
  union    = L.union
  toList   = id
  fromList = id

insertAll :: Collection c a => [a] -> c -> c
insertAll as coll = foldl' (flip insert) coll as


unions :: Eq a => [[a]] -> [a]
unions ll = L.nub (concat ll)

lookupList :: Ord k => k -> M.Map k [v] -> [v]
lookupList node escMap = M.findWithDefault [] node escMap

deleteAll :: (Ord k) => [k] -> M.Map k v -> M.Map k v
deleteAll keys m = foldl' (flip M.delete) m keys 

isSingleton :: [a] -> Bool
isSingleton [_] = True
isSingleton _   = False

-- Print Value
printVal :: (Show a) => String -> a -> IO ()
printVal m v =
  let
    s0 = groom v
    s1 = if elem '\n' s0 then "\n" ++ init (indent 2 s0) else s0
  in
    printStr (m ++ " = " ++ s1)

-- Print String
printStr :: String -> IO ()
printStr m = 
  if doPrintouts then 
    putStrLn m 
  else
    return ()

-- Print New Line
printNl :: IO ()
printNl = printStr ""

indent :: Int -> String -> String
indent i s = unlines (map ((replicate i ' ') ++) (lines s))


printTerm :: Term -> IO ()
printTerm t = printStr ("------ analyseTerm: " ++ show t)

incSiteIx :: SiteIx -> SiteIx
incSiteIx [] = error "Can not inc empty index"
incSiteIx (ch : str) = (chr (ord ch + 1)) : str

zipWithSiteIx :: [a] -> SiteIx -> [(a, String)]
zipWithSiteIx ts siteIx = zip ts (map (\d -> siteIx ++ [d]) (tail digits))


-- List of digit characters, then continuing with all kinds of symbols.
digits :: [Char]
digits = ['0' .. '9'] ++ ['a' .. 'z'] ++ ['!' .. '/'] ++ [':' .. '@'] ++ ['{' .. '~']
 ++ ['[' .. '`'] ++ [error "Taken one too many digit!"]

-- Lookup a key in a map, error if it is not found
lookupBold :: (Show k, Ord k, Show v) => k -> M.Map k v -> v
lookupBold k m =
  case M.lookup k m of
    Just v -> v
    Nothing -> error ("Failed lookupBold of key: " ++ show k ++ " in map: " ++ show m)

assertEq :: (Eq a, Show a) => AlgContext -> a -> a -> String -> IO ()
assertEq cont exp act msg = let
    wholeMsg = 
        "--------------------------------------------------\n" ++
        "### Assertation failure ###\n" ++
        "Test case name: " ++ acTestCaseName cont ++ "\n" ++
        "Site index: " ++ (acSiteIx cont) ++ "\n" ++ 
        msg ++ ": Expected = " ++ show exp ++ ", Actual = " ++ show act
  in
    if exp == act then
      printVal msg act 
    else if (acDoAsserts cont) then
      (error wholeMsg)
    else
      printStr wholeMsg

assertBool :: AlgContext -> Bool -> String -> IO ()
assertBool cont isOk msg = assertEq cont True isOk msg

-- Show a term with nice formatting
groom :: Show a => a -> String
groom s = case parseExp (show s) of
    ParseOk x -> prettyPrintStyleMode 
      Style{ mode = PageMode, lineLength = 150, ribbonsPerLine = 2.5 } defaultMode x
    ParseFailed{} -> show s
-- If haskell-src-exts package is not availible use this version instead:
-- grom = show


-- Test if all elements are equal. Error on empty list.
allEqual :: Eq a => [a] -> Bool
allEqual l = all (head l ==) (tail l)



fixedpointBy :: (a -> a -> Bool) -> [a] -> a
fixedpointBy test (h1 : t1) = go h1 t1 
  where
    go prev (h : t) =
      if test prev h then prev
      else go h t
    go _ _ = error "Calling fixedpointBy with one element list! "

fixedpointBy _ _ = error "Calling fixedpointBy with empty list! "


fixedpoint :: Eq a => [a] -> a
fixedpoint es = fixedpointBy (==) es 


