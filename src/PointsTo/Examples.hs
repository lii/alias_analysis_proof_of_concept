{-# LANGUAGE OverloadedStrings #-}
{-# OPTIONS_GHC -fno-warn-missing-signatures #-}

module PointsTo.Examples (
    module PointsTo.Test,
    module PointsTo.Examples,
  ) where

import PointsTo.Alg
import PointsTo.Data

import PointsTo.Test


ifTest = mkTest{
    tcName = "If",
    tcProg = Method ["p1"] [
      Create "l1",
      If [
        Copy "l1" "p1"
      ] [
        Empty
      ]
    ]
  }

ifTestBlock =
  MethodBlock ["p1"] (Block [
      Create "l1",
      IfBlock
        (Copy "l1" "p1")
        Empty 
    ])

ifGraph = 
  PointsToGraph{
      outEdges = fromList [],
      inEdges = fromList [
          (VarRef "l1", ("M(R(p1) R(1))", fromList [ParNode "p1", InNode "1" True "l1"])),
          (VarRef "p1", ("R(p1)", fromList [ParNode "p1"]))
        ],
      escapeMap = fromList [(ParNode "p1", [VarSite "p1"])],
      returned = fromList []
    }


