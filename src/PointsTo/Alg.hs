module PointsTo.Alg (
    module PointsTo.Alg,
  ) where

import qualified Data.Map as M
import qualified Data.Set as S
import Data.Foldable

import Data.List hiding (any, foldl, all, foldl1, concat, elem, concatMap, union, and, foldl')
import Data.Function
import Control.Monad hiding (join)
import Text.Printf
import qualified Data.ByteString.Char8 as B 

import Prelude hiding (any, foldl, all, foldl1, concat, elem, concatMap, exp, and)

import PointsTo.Data
import PointsTo.Operations
import PointsTo.Util


analyseTerm :: PointsToGraph -> AlgContext -> Term -> IO (PointsToGraph, AlgResult)

analyseTerm graph _c t@(Copy lhs rhs) = do
  printTerm t
  let
    valRhs     = lookupBold (VarRef rhs) (inEdges graph) 
    newInEdges = M.insert (VarRef lhs) valRhs (inEdges graph)
  printVal "newInEdges" newInEdges ; printNl
  return (graph{ inEdges = newInEdges }, emptyResult)

analyseTerm graph cxt t@(Create lhs) = do
  printTerm t
  let
    newRefVal  = RefValue (acSiteIx cxt)
    newNode    = InNode (acSiteIx cxt) (acExecOnce cxt) (mkInDesc lhs)
    newInEdges = M.insert (VarRef lhs) (newRefVal, S.singleton newNode) (inEdges graph)

  printVal "newInEdges" newInEdges ; printNl
  return (graph{ inEdges = newInEdges }, emptyResult)

analyseTerm graph cxt t@(Store ref field rhs) = do
  printTerm t
  let
    nodesLhs = S.toList (pointedToVar (inEdges graph) ref)
    (refRhs, nodesRhs) = lookupBold (VarRef rhs) (inEdges graph)
    genEdges = 
      if isSingleton nodesLhs
         && isCaptured graph (head nodesLhs)
         && isSingular (head nodesLhs) then
         -- Strong update. Don't keep old values. Store old ref value.
        M.singleton (FieldRef (head nodesLhs) field) (refRhs, nodesRhs)
      else M.fromList
        -- Weak update
        [(FieldRef n field,
          (NodeRefValue n (acSiteIx cxt),
          S.union nodesRhs (pointedToField (inEdges graph) n field)))
          | n <- nodesLhs]
    -- Union is left biased so new entries overwrite old.
    newInEdges = M.union genEdges (inEdges graph)
    
  newEscMap <- propagate graph{ inEdges = newInEdges } nodesLhs
  printVal "genEdges" genEdges ; printVal "newEscMap" newEscMap ; printNl
  return (graph{ inEdges = newInEdges, escapeMap = newEscMap }, emptyResult)

analyseTerm graph cxt t@(Load lhs rhs field) = do
  printTerm t
  let
    siteIx             = acSiteIx cxt
    nodesRhs           = pointedToVar (inEdges graph) rhs
    -- If final field then escaped nodes can not be modified. Filter functions.
    modFilter          = if isFinalField cxt field then isOutNode else isEscaped graph
    modNodes           = [n | n <- S.toList nodesRhs, modFilter n]
    -- If final singular nodes will have a special ref value, else all node will have 
    -- statements 'load ref value'. 
    defRefVal          = 
      if isFinalField cxt field then
        (\n -> if isSingular n then FieldRefValue n field else RefValue siteIx)
      else
        (\_n -> RefValue siteIx)
    -- Values of the field on nodes
    (refVals, nodeSets) = unzip
        [M.findWithDefault (defRefVal n, S.empty) (FieldRef n field) (inEdges graph)
          | n <- S.toList nodesRhs]
    nodesField = S.unions nodeSets
    -- If all fields have same ref val take that one, else create new 
    newRefVal  = if allEqual refVals then head refVals else RefValue siteIx

  printVal "modNodes" modNodes ; printVal "nodesField" nodesField ; printNl
  
  if null modNodes then do
    -- No node could have been modified, the simple case
    let 
      newInEdges = M.insert (VarRef lhs) (newRefVal, nodesField) (inEdges graph)
    printVal "newInEdges" newInEdges ; printNl
    return (graph{ inEdges = newInEdges }, emptyResult)
  else do
    let
      loadNode      = LoadNode siteIx (acExecOnce cxt) (mkLoadDesc lhs rhs field)  
      newInEdges    = M.insert (VarRef lhs) (newRefVal, S.insert loadNode nodesField) (inEdges graph)
      outEdgesToAdd = M.fromList [(FieldRef n field, (NodeRefValue n siteIx, S.singleton n)) | n <- modNodes]
      newOutEdges   = 
        M.unionWith (\(_r1, ns1) (r2, ns2) -> (r2, S.union ns1 ns2)) 
          (outEdges graph) outEdgesToAdd
      newEsc      = M.insertWith union loadNode [LoadSite siteIx] (escapeMap graph)
    return (graph{ inEdges = newInEdges, outEdges = newOutEdges, escapeMap = newEsc }, emptyResult)


analyseTerm graph cxt t@(Inv lhs meth args) = do
  printTerm t
  let
    retNode    = RetNode (acSiteIx cxt) (acExecOnce cxt) (mkRetDesc lhs meth args)
    newInNodes = M.insert (VarRef lhs) (RefValue (acSiteIx cxt), S.singleton retNode) (inEdges graph)
    argNodes   = S.toList (S.unions [pointedToVar (inEdges graph) arg | arg <- args])
    invSite    = InvSite (acSiteIx cxt) (mkRetDesc lhs meth args)
    argsEscMap = foldl' (\escMap node -> M.insertWith union node [invSite] escMap)
                   (escapeMap graph)
                   (retNode : argNodes)

  newEscMap <- propagate graph{ inEdges = newInNodes, escapeMap = argsEscMap } argNodes
  return (graph{ inEdges = newInNodes, escapeMap = newEscMap }, emptyResult)


analyseTerm graph0 _cxt t@(Return var) = do
  printTerm t
  let
    newR   = S.toList (pointedToVar (inEdges graph0) var)
    graph1 = graph0{ returned = insertAll newR (returned graph0) }

  return (emptyGraph, emptyResult{ arReturnGraphs = [graph1] })


analyseTerm graph cxt t@(EqConstr var1 var2) = do
  printTerm t
  let
    -- Calculate the nodes in the intersection.
    (val1, nodes1) = lookupBold (VarRef var1) (inEdges graph)
    (val2, nodes2) = lookupBold (VarRef var2) (inEdges graph)
    newRefEntry1   = (mkConstrRefValue val1 val2, interNodes graph nodes1 nodes2)
    newRefEntry2   = (mkConstrRefValue val2 val1, interNodes graph nodes2 nodes1)
    -- Put the constrained value in the map for both variables.
    newInEdges = insertAll
      [(VarRef var1, newRefEntry1), (VarRef var2, newRefEntry2)]
      (inEdges graph)
  return (graph{ inEdges = newInEdges }, emptyResult)
  
analyseTerm graph cxt t@(NotEqConstr var1 var2) = do
  printTerm t
  let
    -- Calculate the nodes in the intersection
    (ref1, nodes1) = lookupBold (VarRef var1) (inEdges graph)
    (ref2, nodes2) = lookupBold (VarRef var2) (inEdges graph)
    -- Put the constrained value in the map for both variables
    -- TODO: Filter out inside nodes that are in both node sets.
    newInEdges = insertAll [
      (VarRef var1, (ExclRefValue (acSiteIx cxt) ref1 ref2, nodes1)),  
      (VarRef var2, (ExclRefValue (acSiteIx cxt) ref2 ref1, nodes2))]
      (inEdges graph)
  return (graph{ inEdges = newInEdges }, emptyResult)

analyseTerm graph0 cxt t@(If thenTerm elseTerm) = do
  printTerm t
  
  printStr "------ Then branch:"
  (graph1, result1) <- analyseBlock graph0 (addSiteIx cxt "1") thenTerm

  printStr "------ Else branch:"
  (graph2, result2) <- analyseBlock graph0 (addSiteIx cxt "2") elseTerm

  return (joinGraph (acSiteIx cxt) graph1 graph2, joinResults result1 result2) 

analyseTerm graph _cxt t@Break = do
  printTerm t
  return (emptyGraph, emptyResult{ arBreakGraphs = [graph] })


analyseTerm graph _cxt t@Continue = do
  printTerm t
  return (emptyGraph, emptyResult{ arContinueGraphs = [graph] })


analyseTerm initGraph cxt0 t@(Loop body) = do
  printTerm t
  (graph1, result1) <- analyseLoop initGraph False 1

  let
    graph2    = joinGraphs (graph1 : arBreakGraphs result1) (acSiteIx cxt0)
    newResult = result1{ arBreakGraphs = [] }

  return (graph2, newResult)

  where
    cxtLoop = cxt0{ acExecOnce = False }
    
    analyseLoop :: PointsToGraph -> Bool -> Int -> IO (PointsToGraph, AlgResult)
    analyseLoop graph0 iterDone iterNr = do
      assertIter cxtLoop iterNr
      printVal ("init_graph_" ++ show iterNr) initGraph
      printVal ("graph_head_" ++ show iterNr) graph0

      -- Call corresponds to putting loop header in work list.
      (graph1, result1) <- analyseBlock graph0 cxtLoop{ acIterDone = iterDone } body

      printVal ("graph_after_body_" ++ show iterNr) graph1

      -- Join of every pred of loop head in CFG: Node at end of body and nodes with continue.
      let joinedGraph = joinGraphs (initGraph : graph1 : arContinueGraphs result1) (acSiteIx cxtLoop)

      printVal ("graph_after_join_" ++ show iterNr) joinedGraph

      if iterDone then
        -- Iteration done and a last pass performed, time to return.
        return (joinedGraph, result1{ arContinueGraphs = [] })
      else if graphsEquiv graph0 joinedGraph (acSiteIx cxtLoop) then
        -- Iteration done for this loop, check surrounding loops.
        if acIterDone cxtLoop then
          -- Iteration done for all surrouding loops also, make one last pass.
          analyseLoop joinedGraph True (iterNr + 1)
        else
          -- Iteration not done for surrouding loops, no last pass, return.
          return (joinedGraph, result1{ arContinueGraphs = [] })
      else
        -- No fixed-point reached, do another iteration
        analyseLoop joinedGraph False (iterNr + 1)


analyseTerm graph _c t@Empty = do
  printTerm t
  return (graph, emptyResult)

-- Debug print the points-to graph
analyseTerm graph _cxt t@PrintGraph = do
  printTerm t ; printNl
  printVal "graph" graph
  return (graph, emptyResult)

-- Debug print whether vars alias
analyseTerm graph _cxt t@(PrintAlias v1 v2) = do
  printTerm t ; printNl
  printVal ("Must Alias " ++ show v1 ++ " " ++ show v2) (mustAlias graph v1 v2)
  printVal ("May Alias "  ++ show v1 ++ " " ++ show v2) (mayAlias  graph v1 v2)
  return (graph, emptyResult)

-- Assertations:
analyseTerm graph cxt t@(AssertNotMay var1 var2) = do
  assertAlias graph cxt t var1 var2 False False

analyseTerm graph cxt t@(AssertMay var1 var2) = 
  assertAlias graph cxt t var1 var2 True False
  
analyseTerm graph cxt t@(AssertMust var1 var2) =
  assertAlias graph cxt t var1 var2 True True

analyseTerm graph cxt t@(Assert exp testFunc) = 
  assertEqVals graph cxt t exp (testFunc graph) "Manual assertation"

analyseTerm _ _ (Block _)      = error "Block"
analyseTerm _ _ (IfBlock _ _)  = error "IfBlock"
analyseTerm _ _ (WhileBlock _) = error "WhileBlock"


-------------------------------------------------------------------------------

analyseBlock :: PointsToGraph -> AlgContext -> [Term] -> IO (PointsToGraph, AlgResult)
analyseBlock graphOrig cxt body = do
  res <- foldM
    (\(graph0, result0) (term, siteIx) -> do
      (graph1, result1) <- analyseTerm graph0 cxt{ acSiteIx = siteIx } term
      return (graph1, joinResults result0 result1))
    (graphOrig, emptyResult)
    (zipWithSiteIx body (acSiteIx cxt))
  return res


-------------------------------------------------------------------------------

-- TODO: This still kind of use list representation
propagate :: PointsToGraph -> [Node] -> IO (M.Map Node [EscapeSite])
propagate graph nodes = do
  printStr "------ propagate:"
  result <- doWhileWork nodes (escapeMap graph)
  printStr "------ propagate end" ; printNl
  return result

  where
    -- Convert edge maps to set-of-pairs representation 
    allEdges :: [(Ref, Node)]
    allEdges = concatMap (\(ref, (_refVal, ns)) -> [(ref, n) | n <- S.toList ns])
      (M.toList (outEdges graph) ++ M.toList (inEdges graph))
    
    -- Loop while there are nodes in the work list
    doWhileWork :: [Node] -> M.Map Node [EscapeSite] -> IO (M.Map Node [EscapeSite])
    doWhileWork []                escMap  = return escMap
    doWhileWork (node : workList) escMap0 = do
      printVal "node" node
      printVal "workList" workList
      printVal "escMap0" escMap0

      -- For each egde from current node, propagate escaped status 
      (escMap1, newWorkList) <- foldM
        (\(escMapLoc, workListLoc) (FieldRef srcNode _f, destNode) -> do
          let destNodeEsc    = lookupList destNode escMapLoc
              newDestNodeEsc = union destNodeEsc (lookupList srcNode escMapLoc)
          return $
            if destNodeEsc == newDestNodeEsc
              then (escMapLoc, workListLoc)
              else (M.insert destNode newDestNodeEsc escMapLoc, destNode : workListLoc))

        (escMap0, [])
        (edgesFromNode allEdges node)

      doWhileWork (workList ++ newWorkList) escMap1

-------------------------------------------


analyse :: Method -> [Var] -> [Field] -> Int -> TestCase -> IO PointsToGraph
analyse (Method params body) classes finals maxIter tCase = do
  let 
    cxt0 = AlgContext{
        acSiteIx        = "",
        acMaxLoopIter   = maxIter,
        acExecOnce      = True,
        acIterDone      = True,
        acFinalFields   = S.fromList finals,
        acDoAsserts     = tcDoAsserts tCase,
        acTestCaseName  = tcName tCase
      }
  
    nodeVars = zip 
      ([(ParNode var, var) | var <- params] ++ [(ClsNode var, var) | var <- classes])
      [B.unpack i | Var (Id i) <- params ++ classes]

    graph0 = PointsToGraph{
        outEdges  = M.empty,
        inEdges   = M.fromList [(VarRef var, (RefValue uid, S.singleton node)) |
                    ((node, var), uid) <- nodeVars],
        escapeMap = M.fromList [((node, [VarSite var])) | ((node, var), _) <- nodeVars],
        returned  = S.empty
      }

  (graph1, result) <- analyseBlock graph0 cxt0 body

  return (joinGraphs (graph1 : arReturnGraphs result) (acSiteIx cxt0))


-------------------------------------------------------------------------------

-- Code for testing and debugging
  
assertAlias :: PointsToGraph -> AlgContext -> Term -> Var -> Var -> Bool -> Bool
  -> IO (PointsToGraph, AlgResult)
assertAlias graph cxt term var1 var2 expMay expMust =
  if acIterDone cxt then do
    assertEqVals graph cxt term (expMay, expMust)
      (mayAlias  graph var1 var2, mustAlias graph var1 var2)
      ("(May, Must) alias " ++ show var1 ++ " " ++ show var2)
  else
    return (graph, emptyResult)

assertEqVals :: (Eq a, Show a) => 
  PointsToGraph -> AlgContext -> Term -> a -> a -> String -> IO (PointsToGraph, AlgResult)
assertEqVals graph cxt term exp real msg = do
  printTerm term ; printNl
  assertEq cxt exp real msg 
  return (graph, emptyResult)

assertIter :: AlgContext -> Int -> IO ()
assertIter cxt iterNr = do
  let maxNrIter = acMaxLoopIter cxt
  printStr ("---- analyseLoop iterNr: " ++ show iterNr)
  assertEq cxt True (iterNr < maxNrIter) ("-- Loop iterations < " ++ show maxNrIter)

